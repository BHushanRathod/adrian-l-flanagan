#!/usr/bin/env python3
"""A command to check a rendered HTML page against a validator service."""

from django.core.management.base import BaseCommand, CommandError
from validator.html5check import call_service
from django.shortcuts import render


class Command(BaseCommand):
    """manage.py command to check HTML pages with a validator service."""
    help = 'Renders HTML pages and checks them with the service http://validator.w3.org/nu/.'

    def add_arguments(self, parser):
        """Set up arguments for new admin.py command."""
        parser.add_argument('URL', nargs='+')

    def handle(self, *args, **options):
        """For each url in `options['URL']`, validate that url against online service."""
        for url in options['URL']:
            response = render(None, url)
            data = response.content
            call_service(data, 'text/html', encoding='utf-8',
                         service='http://html5.validator.nu/', errors_only=False)
