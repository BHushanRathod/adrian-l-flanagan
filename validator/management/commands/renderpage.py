#!/usr/bin/env python3
"""A command to render a specific URL to standard out."""

from django.core.management.base import BaseCommand, CommandError
from django.shortcuts import render


class Command(BaseCommand):
    """manage.py convienence command to render a page to stdout."""
    help = 'Renders HTML page to standard output'

    def add_arguments(self, parser):
        """Set up arguments for new admin.py command."""
        parser.add_argument('URL', nargs='+')

    def handle(self, *args, **options):
        """For each url in `options['URL']`, render it, then print the result."""
        for url in options['URL']:
            response = render(None, url)
            self.stdout.write(response.content)
