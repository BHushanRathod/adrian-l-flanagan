"""
WSGI config for adrian_l_flanagan project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/

For use of this file in a Heroku environment, see
https://devcenter.heroku.com/articles/getting-started-with-django#django-settings

"""

import os

from django.core.wsgi import get_wsgi_application
from whitenoise.django import DjangoWhiteNoise

from dj_static import Cling


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "adrian_l_flanagan.settings")

application = Cling(get_wsgi_application())
application = DjangoWhiteNoise(application)
